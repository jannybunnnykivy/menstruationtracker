import datetime
import os
from dataclasses import dataclass

from kivy.uix.behaviors import ToggleButtonBehavior
from sqlalchemy import asc
from sqlalchemy import Column
from sqlalchemy import create_engine
from sqlalchemy import DateTime
from sqlalchemy import Integer
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

engine = create_engine("sqlite:///monthlydb.sqlite3", echo=True)
Base = declarative_base(bind=engine)


@dataclass
class DiaryEntry(Base):
    __tablename__ = "measurements"
    """Class for keeping track of an entry in diary."""
    created_date = Column(DateTime, primary_key=True)
    strength = Column(Integer, default=0)
    sex = Column(Integer, default=0)
    mood = Column(Integer, default=0)
    symptoms = Column(Integer, default=0)
    discharge = Column(Integer, default=0)
    others = Column(Integer, default=0)


@dataclass
class Menstruation:
    start: datetime
    duration: int


# Initialize database if it doesn't exist
if not os.path.exists("monthlydb.sqlite3"):
    Base.metadata.create_all(engine)


class MonthlyDB:
    con: sessionmaker

    def __init__(self):
        session_factory = sessionmaker(bind=engine)
        self.con = session_factory()

    def addEntry(self, entry: DiaryEntry):
        self.con.merge(entry)
        self.con.commit()

    def update_db(self):
        tbBehaviour = ToggleButtonBehavior.get_widgets("day_num")
        for tbn in tbBehaviour:
            if tbn.is_pressed:
                self.con.merge(
                    DiaryEntry(
                        created_date=tbn.get_date(),
                        strength=0,
                        sex=0,
                        mood=0,
                        symptoms=0,
                        discharge=0,
                        others=0,
                    )
                )
            else:
                self.con.query(DiaryEntry).filter(
                    DiaryEntry.created_date == tbn.get_date()
                ).delete()
        del tbBehaviour
        self.con.commit()
        return

    def get_all(self):
        mens = []
        all_mens = (
            self.con.query(DiaryEntry)
            .order_by(asc(DiaryEntry.created_date))
            .all()
        )
        current_duration = 0
        for i in range(len(all_mens) - 1):
            duration = all_mens[i + 1].created_date - all_mens[i].created_date
            if duration.days > 7 or i == len(all_mens) - 2:
                if i == len(all_mens) - 2:
                    mens.append(
                        Menstruation(
                            start=all_mens[i].created_date, duration=-1
                        )
                    )
                else:
                    mens.append(
                        Menstruation(
                            start=all_mens[i].created_date,
                            duration=duration.days + current_duration,
                        )
                    )
                current_duration = 0
            else:
                current_duration += 1
        return mens
