from kivy.uix.boxlayout import BoxLayout

from .KivyCalendar.calendar_ui import CalendarWidget


class CalendarPanel(BoxLayout):
    def __init__(self, **kwargs):
        super(CalendarPanel, self).__init__(**kwargs)
        self.calendar = CalendarWidget()
        self.build()

    def build(self):
        self.add_widget(self.calendar)
        return self
