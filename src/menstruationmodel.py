import datetime

import numpy as np

from .monthlydb import MonthlyDB


class MenstruationModel:
    menstruation_array: list
    duration_average: float
    duration_std: float

    def __init__(self):
        mdb = MonthlyDB()
        self.menstruation_array = mdb.get_all()
        dur = [o.duration for o in self.menstruation_array]
        self.duration_average = np.mean(dur[:-1])
        self.duration_std = np.std(dur[:-1])
        mdb.con.close()

    def get_ovulation_date(self):
        if len(self.menstruation_array) < 2:
            return 0
        latest_date = self.menstruation_array[-1].start
        return latest_date + datetime.timedelta(
            days=-5.2835 + 0.7344 * self.duration_average
        )

    def get_menstruation_date(self):
        if len(self.menstruation_array) < 2:
            return 0
        latest_date = self.menstruation_array[-1].start
        return latest_date + datetime.timedelta(
            days=self.duration_average - self.duration_std / 2
        )
