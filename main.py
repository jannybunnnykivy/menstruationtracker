from kivy.app import Widget
from kivy.config import Config
from kivy.core.window import Window
from kivy.properties import ObjectProperty
from kivymd.app import MDApp

from src.calendarpanel import CalendarPanel
from src.menstruationmodel import MenstruationModel
from src.monthlydb import MonthlyDB

# from kivymd.uix.picker import MDDatePicker
Config.set("kivy", "exit_on_escape", "0")


class Monthly(Widget):
    calendar = ObjectProperty(None)


class MonthlyApp(MDApp):
    cal: CalendarPanel

    def __init__(self, **kwargs):
        super(MonthlyApp, self).__init__(**kwargs)

    def build(self):
        Window.bind(on_request_close=self.on_request_close)
        self.theme_cls.theme_style = "Dark"
        self.theme_cls.primary_palette = "BlueGray"
        return Monthly()

    def on_start(self):
        self.run_model()
        return super().on_start()

    def on_request_close(self, *args):
        mdb = MonthlyDB()
        mdb.update_db()
        mdb.con.close()
        self.stop()
        return True

    def run_model(self):
        model = MenstruationModel()
        ovu_time = model.get_ovulation_date()
        if ovu_time != 0:
            self.root.ids.ovulation_text_box.text = ovu_time.strftime(
                "%d %B %Y, %H:%M:%S"
            )
        mens_time = model.get_menstruation_date()
        if mens_time != 0:
            self.root.ids.menstruation_text_box.text = mens_time.strftime(
                "%d %B %Y, %H:%M:%S"
            )


if __name__ == "__main__":
    MonthlyApp().run()
